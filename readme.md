# Molflow and Synrad binaries

This repo has been superseded by the CERNBox folder: <https://cernbox.cern.ch/s/3kHO7X55GXNa7b9>

For auto-built Molflow 2.9+ releases, go to the Gitlab repo: <https://gitlab.cern.ch/molflow_synrad/molflow/-/releases>