* Molflow+ 2.6.77 for Windows (2019 mar 27)

* Molflow+ 2.6.76 for Windows (2019 feb 25)

* Molflow+ 2.6.75 for Windows (2019 feb 21)

* Molflow+ 2.6.74 for Windows (2019 feb 5)

* Molflow+ 2.6.72 for Windows (2018 sep 3) 

* Molflow+ 2.6.71 (2018 aug 16) 

* Molflow+ 2.6.70 (2018 jul 6) 

* Molflow+ 2.6.69 (2018 may 24) 

* Molflow+ 2.6.68 (2018 may 16) 

* Molflow+ 2.6.67 (2018 may 14) 

* Molflow+ 2.6.66 (2018 apr 10) 

* Molflow+ 2.6.65 (2018 apr 5) 

* Molflow+ 2.6.64 (2018 jan 25) 

* Molflow+ 2.6.62 (2017 nov 30) 

* Molflow+ 2.6.61 (2017 nov 29) 

* Molflow+ 2.6.60 (2017 nov 16) 

* Molflow+ 2.6.59 (2017 oct 30) 

* Molflow+ 2.6.57 (2017 oct 12)

* Molflow+ 2.6.55 (2017 sep 28) 

* Molflow+ 2.6.53 (2017 aug 24) 

* Molflow+ 2.6.52 (2017 aug 1) 

* Molflow+ 2.6.49 (2017 may 8) 

* Molflow+ 2.6.48 (2017 apr 13) 

* Molflow+ 2.6.45 (2017 apr 5) 

* Molflow+ 2.6.44 (2017 mar 28) 

* Molflow+ 2.6.43 (2017 mar 21) 

* Molflow+ 2.6.41 (2017 mar 2) 

* Molflow+ 2.6.40 (2017 feb 23)

* Molflow+ 2.6.39 (2017 feb 22) 

* Molflow+ 2.6.36 (2017 jan 23) 

* Molflow+ 2.6.33 (2016 oct 4)

* Molflow+ 2.6.32 (2016 sep 5)

* Molflow+ 2.6.31 (2016 aug 30) 

* Molflow+ 2.6.30 (2016 aug 20) 

* Molflow+ 2.6.29 (2016 aug 15) 

* Molflow+ 2.6.28 (2016 jul 18) 

* Molflow+ 2.6.25 beta (2016 mar 8) 

* Molflow+ 2.6.23 beta (2015 dec 17)

* Molflow+ 2.6.19 beta (2015 oct 21)

* Molflow+ 2.6.17 beta (2015 oct 19) 

* Molflow+ 2.6.13 beta (2015 july 7) - 64bit 32bit Mac

* Molflow+ 2.6.12 64-bit beta (2015 june 5)

* Molflow+ 2.6.11 64-bit beta (2015 may 13)

* Molflow+ 2.6.10 64-bit beta (2015 may 12)

* Molflow+ 2.6.9 64-bit beta (2015 apr 30)

* Molflow+ 2.6.8 64-bit beta (2015 apr 16)

* Molflow+ 2.6.7 64-bit beta (2015 apr 2)

* Molflow+ 2.6.6 64-bit beta (2015 mar 11)

* Molflow+ 2.6.5 64-bit beta (2015 mar 5)

* Molflow+ 2.6.3 64-bit beta (2015 jan 27)

* Molflow+ 2.6.1.3 beta (2014 dec 18)

* Molflow+ 2.6.1 beta (2014 oct 6) - Windows Mac

* Molflow+ 2.6 beta (2014 sep 18)