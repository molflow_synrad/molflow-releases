2020-09-21

# Fix

- Fixed longer loading times under Windows
- Fixed a false notification of a subprocess timeout