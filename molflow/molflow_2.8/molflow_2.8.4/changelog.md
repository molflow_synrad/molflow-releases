2020-11-04

# Feature

- Convergence plotter (showing evolution of formula values as simulation runs)

# Interface

- More verbose progressbars when loading large geometries

# Bugs fixed

- Occasional freezes on simulation start after changing geometry
- False timeout warnings for long load times
- Facet advanced parameters: wrong mixed state display
- Results missing when saving old XML format
- Texture autoscaling “jumping around”
- Crash when selecting “previous structure (Ctrl+F11)” with all structures displayed
- macOS: missing slow zoom (shift+mousewheel)
- macOS: displaying CTRL in labels instead of CMD