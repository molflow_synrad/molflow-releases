2020-09-02

# Feature

*   Histograms saved/loaded with XML
*   Triangulate geometry on selected facets
*   Support of non-square texture cells
*   Add/remove ranges of facets in profile plotter
*   Parameters support lin-log, log-lin and log-log interpolation
*   Time moments can have individual time windows
*   Performance optimization when using many time moments

# Interface

*   3D Viewer: Better highlighting for selected facets
*   3D Viewer/Profile Plotter: Colorcoded highlighting for plotted profiles
*   Profile Plotter: Colorblind (and B/W compatible) profiles
*   FacetID view option (facet number in center)
*   Files with long paths abbreviated in Recent menus

# Fix

*   XML save files now have root element (disable for backwards compatibility -> Global Settings)
*   Fixed missing lighting when loading memorized views
*   When inserting geometries, teleport numbers are correctly offset
*   Allow again to insert latest (v10) Synrad SYN files
*   Histogram: flight time now distinguished from particle’s system time
*   Split, Explode and Collapse functions maintain per-area outgassing