2.8.9 (2022-11-14)

### Feature

- Ported near-instant STL collapse and Smart Select from 2.9 beta
- Support multibody STL files
- When deleting facets, teleports are also renumbered