2022-06-30

# Feature

- Can insert v12 Synrad files


# Bugfix

- STL insert works again
- Collienarity check (null facets) synced with Synrad
- Fixed desorption import from Synrad
- Fixed error reporting (meaningful messages restored)