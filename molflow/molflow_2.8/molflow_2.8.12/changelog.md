# Changelog 'v2.8.12'

### Feature
- Immediate ZIP compression without intermediate XML

### Interface
- Support for progressbars with wide/multiline status
- Allow computer to use lock / screensaver while MolFlow is running
- Don't allow 0K temperature

### Bugfix

- Fixed convergence plotter interface elements becoming black rectangles
- Fixed PSD import lin-lin extrapolation instead of log-log