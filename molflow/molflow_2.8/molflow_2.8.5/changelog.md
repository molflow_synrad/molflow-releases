2020-12-17

# Feature

- Allow up to 64 subprocesses (previously 32)

# Interface

- Allow dragging with D key and left mouse button hold
- Allow zooming with Z key and left mouse button hold
- "Select plotted" button in profile plotter

# Bugfix

- Infinite loop when loading a file with invalid convergence values
- Crash when using a time-dependent parameter with a t=0 value
- Very long simulation update time after changing a parameter and restarting
- Max. reported number of CPU cores now not capped at 16
- Incorrect values when exporting velocity vector type textures
- Crash when deleting a structure which is selected
- Crash after using right-click context menu in Texture plotter window
- Autosave recovery "Skip" button acted as "Cancel"