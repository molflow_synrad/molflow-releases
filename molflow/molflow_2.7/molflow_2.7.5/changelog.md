2019-02-25

*   Fixed bugs with parameter editor, parameter catalog
*   Fixed a bug when clicking "Import CSV" in dynamic angle maps
*   Fixed a crash when using dynamic angle maps