2019-04-02

# Interface

*   Profile, Timewise and Histogram plotters now always choose the first available color for new curves
*   Purple highlighting of non-planar facets can be disabled
*   Histogram plotter is now a tabbed window

# Bugfix

*   Some file extensions were not selectable for opening on Mac
*   Fixed mouse wheel closing combobox lists
*   Fixed isolated vertice deletion on geometry insert
*   Profile plotter loads results again on loading new file, even if already open