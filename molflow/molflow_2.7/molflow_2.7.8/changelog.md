2019-05-17

Recompiled with GCC9.1, as Homebrew now installs that version (earlier versions depend on GCC 8.2).

Existing users: update your GCC package by running

`brew update gcc`

in the macOS terminal.