2020-05-15

# Feature

- Added support to triangulate geometries and save geometries in STL files

# Bugfix

- Fixed an issue where saved camera angles could cause Molflow to hang
- Fixed a crash when Profile plotter had memorized views
- Fixed a bug where multiple threads yielded the same results
- Fixed a bug where applying a remesh resulted in an invalidate file state