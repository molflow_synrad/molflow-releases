2019-08-03

# Bugfix

*   Updating plotter windows when deleting facets, fixing previous crash when referring to non-existing facets
*   Fixed memory leak when deleting plots from Pressure Evolution window