2019-12-20

# Feature

* Ability to manually restore flipped normals for old geometries saved with Molflow 2.6.71 or earlier (see new command in Facet menu)

# Bugfix

* Restored compatibility with earlier versions of MacOS