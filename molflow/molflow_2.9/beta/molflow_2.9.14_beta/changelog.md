# Changelog 'v2.9.14_beta'

### Feature

- Load status window for simulation-interface operations that take >0.5s
- Possibility to abort simulation start/stop/reload if taking too long

## Interface

- Resizable Global Settings window (for long thread list)
- Approximation for Sim.Manager and thread memory usage

## Bugfix

- Formula parsing fixed for consecutive multiplications/divisions
- Convergence data saved again
- Prevent crash if convergence data is expected but loaded file hasn't