# Changelog 'v2.9.11_beta'

### Feature

- Faster geometry initialization (after load, move, scale etc.)

### Bugfix

- Restored loading of textures
- Prevented some race conditions