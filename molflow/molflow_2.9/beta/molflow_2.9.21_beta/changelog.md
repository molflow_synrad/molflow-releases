# Changelog 'v2.9.21_beta'
2024-02-20

### Feature

- molflowCLI: allow 'specific outgassing' parameter change

### Interface

- Histogram settings panel shows memory estimate of histograms
- Show actual desorption limit in Global Settings on change button
- Sanitize desorption limit user input before applying
- molflowCLI: feedback (success/error) for parameter changes
- Easier to understand Texture Scaling window controls
- Select by texture type: allow 0 as min. ratio

### Bugfix

- MolFlow runs on OpenGL1.1 environments again (i.e. Remote Desktop servers)
- Selection sometimes not finding facets on click fixed
- Inserting geometries: formula and selection group offsetting working again
- Stop simulation correctly if desorption limit reached almost instantly after start
- molflowCLI: allow to run simulations for very small desorption limits
- molflowCLI: --setParamsByFile with outgassing: fixed factor of 10 error
- crash after Copy Mirror command
- every second line empty on Texture Plotter export