# Changelog 'v2.9.13_beta'

### Feature

- Support binary STL files (single body, load only)
- Formulas can refer to formulas defined above
- Progress printed for file saving (GUI and CLI)
- Google Analytics replaced with Matomo (GDPR)

### Bugfix

- Include missing vcomp140.dll for OpenMP parallelization on Windows
- Fixed incorrect filename check saying xml not supported