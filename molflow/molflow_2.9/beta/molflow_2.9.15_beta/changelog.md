# Changelog 'v2.9.15_beta'

### Feature

- Facet temperatures can be time-dependent (non-outgassing only)
- Lowered XML file memory usage by ~25%
- XML format change: refer to parameters by name, not by ID
    - Backwards compatible with previous versions, except for temperature

### Interface

- Time-dependent sticking, opacity and temp. now shown in Facet details
    - In blue color, value for current moment, as defined in Time Settings
- Total outgassing now calculated by simulation model (as opposed to GUI)
    - Needs manual recalc. button push if model not synced
- Model sanity check now also done by simulation model

### Bugfix

- Fixed hang when exiting Molflow with simulation running