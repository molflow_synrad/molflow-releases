# Changelog 'v2.9.16_beta'

## Interface

- Message boxes (error and confirmation) now accept Enter and Esc
- Global settings right pane now shows that settings are file-based

## Bugfix

- Desorption limit working again (GUI & CLI)
- No more false warnings for app updater
- Low-flux mode and gas mass saved with file, not with app settings