# Changelog 'v2.9.12_beta'

### Bugfix

- Hotfix for bug causing updater to crash. Update manually if updater fails.