Molflow 2.9.0 beta

2021-09-30

# Feature

- Added CLI application (see documentation)
- Improved simulation performance with major rework of the simulation framework
- Support dynamic outgassing from non-square facets
- Added MacOS ARM support for M1 chips
- Added support for multibody STL files

# Minor changes

- Some usability changes for several tool windows
- Removed texture limits from output file
- Renamed "overwrite" to "save/overwrite" for "View" and "Selection" menus

# Developers

- Added google-test support

# Bugfix

- Fixed proper distribution of desorptions on all CPU cores when a limit is set
- Log/Log interpolation used when importing desorption yield for dynamic desorption
- Fixed double to double comparison in Facetdetails
- Keep child windows on screen when size of main window is reduced
- Fix crash when bounding box of the geometry had 0-length side
- Fixed some bugs when files contained "." characters or had no extension
- Fixed some issues concerning import and export from STL files