# Changelog 'v2.9.18_beta'
2024-01-08

### Feature

- Cross section viewer (find in View menu)
- Last camera view restored when loading a file (if saved with 2.9.17+)
- Texture scaling settings (auto/manual limits) saved and loaded again
- Security: hard-code updater URL to avoid tampering with updater_config.xml
- Security (macOS, Linux): disable custom post-update scripts,
  restrict to give executable permission by code
- Particle logger: all processes add to log (previously only first)

### Interface

- Formula editor: allow to batch copy values for all moments
- Auto-update formulas moved from Global Settings to Formula Editor
- Easier to understand molflowCLI status messages and help
- Allow "-inf" and "+inf" texture limits if no textures (or no moments)

### Bugfix

- Histogram plotter had no values for global histograms and for facet-based flight time
- Angle map CSV import now allows 64-bit unsigned integers (previously max 2^31-1)
- Full screen mode works again
- Moments load progressbar off-by-one error
- STL loading: better handle scientific numbers, give meaningful error messages
- Clear formulas on File/New geometry
- Hotfix after 2.9.17 broken formula parsing