# Features

*   [Autosave (always / only when simulation is running)](https://molflow.web.cern.ch/node/55)
*   [Saved files are compressed in background with 7-Zip](https://molflow.web.cern.ch/node/56) (.geo7z container)
*   Profiles are now saved and loaded from GEO files
*   [Collienar facets are detected and can be deleted](https://molflow.web.cern.ch/node/57)
*   [Geometry editor: rotation](https://molflow.web.cern.ch/node/58)
*   [Geometry editor: align](https://molflow.web.cern.ch/node/59)
*   Geometry editor copy functions now copy mesh parameters too
*   molflowSub.exe processes self-close when Moflow is not running anymore
*   Geometry is passed to sub-processes only when necessary (faster facet parameter changes)
*   Vertex collapse routine is now about ~40% faster
*   Version 1 (very old) GEO files can now be loaded

# Interface

*   [Selections (facet groups) can be memorized, they are also saved / loaded from GEO files](https://molflow.web.cern.ch/node/60)
*   [Process control is moved to Global Settings](https://molflow.web.cern.ch/node/61)
*   Facet hits panel (lower right) can now be sorted by clicking the column labels
*   Mouse selection is more responsive for large systems: progressbar / hourglass cursor added
*   CAPS LOCK restricts selectable facets to those with a selected vertex
*   Profile Plotter can select the plotted facet
*   Recent... file menu is now in chronological order
*   Facet parameters panel reordered (particle input / output separated)
*   Autoscale button now keeps being turned on (toggle)
*   Blue gradient background
*   On startup, only one viewer is visble

# Bugfixes

*   Corrected a calculation error where pressure was divided by the square root of the tempereature
*   Corrected a bug which gave "Invalid outgassing rate" error on some facets
*   Fixed a bug where Molflow crashed after removing facets with a plotted profile
*   Average pressure is now correctly displayed on facets with 0 opacity
*   OpenGL corrections (edge bleeding / Z-fighting problems fixed)