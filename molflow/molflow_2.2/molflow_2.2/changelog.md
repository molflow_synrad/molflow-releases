2012-05-16

# Features

*   Hits and textures are saved in .GEO files
*   Textures of selected facets can be exported to a TXT file (File -> Export selected textures)
*   [Pressure is displayed in facet details dialog](https://molflow.web.cern.ch/node/5)
*   [New profile plotter option: normalize to pressure](https://molflow.web.cern.ch/node/4)
*   [Volumetric flow rate calculated for the selected facet](https://molflow.web.cern.ch/node/3)
*   [Sticking ratio can be set by entering desired volumetric flow rate](https://molflow.web.cern.ch/node/3)
*   [Sum area of selected facets displayed](https://molflow.web.cern.ch/node/2)
*   New setting: number of displayed 'Lines' (particle trajectories)
*   Added a quick test pipe option (Test -> Quick test pipe)

# Interface

*   [Open / Save dialogs changed to standard Windows dialogs](https://molflow.web.cern.ch/node/12)
*   Middle mouse button moves camera
*   ALT+left mouse button moves camera
*   Mouse wheel zoom speed increases while CTRL is pressed
*   Non editable text- and combo boxes are greyed out
*   Structure menu: CTRL-F11 and CTRL-F12 cycles through structures

# Bug fixes

*   Fixed a bug where STL files didn't load
*   Fixed a bug: loading files with very long lines
*   File reading now gives error messages instead of crashing Molflow+
*   [Facets without textures now display the volume](https://molflow.web.cern.ch/node/6)