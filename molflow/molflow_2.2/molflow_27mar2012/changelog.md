**Different from ver. 2.2**

*   2-sided facets now have 2-sided desorption
*   Slow zooming changed to Ctrl + Mouse Wheel
*   Fast zooming changed to Shift + Mouse Wheel

**New Features**

*   New tool: vertex selector
*   Select coplanar vertices on screen
*   Create convex polygon from selected vertices
*   Move vertices by offset
*   New facet parameter: "teleport" (for periodic boundary condition)
*   Hit, line and leak caches (last 2048 visible on screen) are now saved
*   Autocorrect non-simple facets (upon load file & collapse)
*   New collapse option: collinear side merge
*   Isolated vertices removed after collapse
*   Facets’ volume/texture display can be toggled individually (Mesh menu)
*   Saving a file inserts it in the "Load recent" list

**Interface**

*   Confirmation dialogs for discarding unsaved changes
*   Antialiasing for facet wireframes (Edit / Global settings)
*   White background option (Edit / Global settings)
*   “.geo” file extension added automatically at save
*   Calculate flow speed / sticking ratio as you type
*   Facet list: Shift + up/down selects multiple rows
*   Option for larger hits
*   Texture viewer: 'Find max' button
*   Start/Stop/Reset simulation buttons reworked to reflect simulation state
*   Number of displayed leaks adjustable

**Bugs**

*   Copy All / Copy selected now works correctly in facet list
*   Flow speed calculation fixed
*   L/R=100 test tube size corrected
*   Test tube transition probability formulas corrected
*   Texture Scale MIN value doesn't show bad data when no texture present
*   Recent file list can now display long filenames