Synrad 1.4.27
2020-09-02

# Feature

- Added support for macOS and Linux
- Triangulate geometry on selected facets
- Regions: Curved quadrupole support

# Interface

- 3D Viewer: Better highlighting for selected facets
- Region Editor/Info: Filenames with long paths scrolled to end by default
- FacetID view option (facet number in center)
- Files with long paths abbreviated in Recent menus

# Fix

- Fixed "Ideal beam" not checked on loading
- Fixed freeze when loading memorized views
- Fixed missing lighting when loading memorized views
- Fix "Apply texture" button setting incorrect flags
- Fixed texture record flags loading in incorrect state
- Fixed textures not recording on reflection
- When inserting geometries, teleport numbers are correctly offset

Note: the updater config file will be rewritten for cross-platform compatibility.
You will see a "Check for updates?" dialog again, only one time.