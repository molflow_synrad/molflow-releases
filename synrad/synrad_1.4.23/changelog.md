Synrad 1.4.23
24-05-2018

- Fixed a memory management error causing crash on geometry change
- Added About/License info