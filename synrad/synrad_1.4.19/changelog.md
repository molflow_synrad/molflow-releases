Synrad 1.4.19
date="29-11-2017

# Bugfix

- Wrong relaunch angle at teleport
- Fixed not saving "shade lines" and "show teleports" viewer settings