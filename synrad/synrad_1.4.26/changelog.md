Synrad 1.4.26
2020-01-29

# Feature

- Combined function magnets (dipole and quadrupole)

# Bugfixes

- Fixed incorrect display and collision finding with concave facets
- Added command to flip normal of old geometries
- Fixed bug when trying to load non-existing ZIP file