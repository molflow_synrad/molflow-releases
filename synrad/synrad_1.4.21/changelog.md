Synrad 1.4.21
14-05-2018

# Feature

- All profiles and spectra count incident and absorbed flux/power/hits separately
- Added Particle Logger tool (Tools menu)
- Screenshot tool (CTRL+R)
- Starting structure changeable for each region
- SYN file advanced to version 10, earlier versions won't open files written with this version

# Interface

- Support for right-handed coordinate system (change in Global Settings)
- Region editor: can copy beam settings from an other region

# Bugfix

- Collapse bug fix
- Removed rough surface reflection angle limit (discrete lines in the reflection pattern)
- Checking for filename conflicts before saving to a syn7z file
- Fixed enabled/disabled controls in region editor
- Fixed "invalid autocorrelation length" message
- BXY file empty lines are skipped (previously gave error)
- Fixed inserting SYN file
- param file handle released if loading fails
- App updater can handle file write errors (crashed before)