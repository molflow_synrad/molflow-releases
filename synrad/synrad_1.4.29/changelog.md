Synrad 1.4.29
2021-01-04

# Feature

- Added Convergence plotter
- Support for multiple bodies in one STL file

# Interface

- Fix for mesh mixed state
- Allow dragging with D key and left mouse button hold
- Allow zooming with Z key and left mouse button hold
- "Select plotted" button in profile plotter

# Bugfix

- Closing collapse/smart select windows aborts the process
- More verbose error message in case of Subprocess error
- Crash when deleting a structure which is selected
- Restore shift+scroll for Mac
- Fixed timeout when loading large files