Synrad 1.4.20
date="30-11-2017

# Feature

- For integrity, spectrum and profiles are only recorded for the absorbed part of flux

# Bugfix

- Collapse function works again
- Spectrums, profiles work again
- Right-click menus work again