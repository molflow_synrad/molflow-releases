Synrad 1.4.22
16-05-2018

# Bugfix

- Fixed hard visibility (with Texture view on) of selected facets in 2.6.67
- Fixed a crash when trying to delete non-existent structure