Synrad 1.4.24
21-12-2018

# Feature:

- Support for facets belonging to all structures at once

# Bugfix:

- Support for compressing files with hundreds of regions
- Apply button becomes active on Spectrum recording toggle
- Critical energy calculation more precise
- Fixed crash when pressing Cancel at Load region to  dialog