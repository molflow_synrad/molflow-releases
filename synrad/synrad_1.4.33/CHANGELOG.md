# Changelog

## v1.4.33 (2023.12.08)

### Feature

- Particle logger allows to record only absorbed hits

### Interface
- Support for progressbars with wide/multiline status

### Bugfix
- Fixed convergence plotter elements becoming black rectangles
- Better estimation for flux/power in particle logger export

## v1.4.32 (2023.02.17)

### Bugfix
- Fixed spectrum saving
- Undo command works again (build intersection, split)
- STL insert fix, display collapse dialog

## v1.4.31 (2023.01.11)
### Feature
- Add S global coordinate to regions
- Allow to refer BXY file to global S beam coordinate
- Allow global and local magnetic field component definition (see docs)
- Much faster collapse and smart select
### Bugfix
- SR pattern rotates with magnetic field (doesn't assume Y is up)
- Various bugfixes from beta during 2021-2022

## v1.4.30 (2021-02-17)

### Feature
- Support for non-square textures
### Bugfix
- Load STL files works again