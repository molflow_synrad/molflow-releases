Synrad 1.4.28
2020-09-21

# Bugfix

- Fixed longer loading times under Windows
- Fixed a false notification of a subprocess timeout
- Fixed a leaks happening when simulation rough surface reflection