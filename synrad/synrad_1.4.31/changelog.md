Synrad 1.4.31 (2023-01-10)

# Feature

- Added global S coordinate to regions BXY files can refer to it)
- Possibility to define local Bx, By, Bz components (relative to reference dir)
- Support for multibody STL files
- Very fast vertex and facet collapse and smart select routines

# Bugfix

- SR fan rotates with magnetic field (doesn't assume global Y direction is up)
- Many bugfixes during 2021 and 2022 ported from MolFlow