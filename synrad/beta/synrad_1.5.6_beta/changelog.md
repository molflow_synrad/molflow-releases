2023-02-17
# Changelog 'v1.5.6_beta'
### Bugfix
- Fixed saving textures with wrong facet index
- Fixed CLI compression of syn7z files
- Fixed Undo command for Split, Build intersection and Align
- Fixed spectrum saving
### Interface
- Better log language
- Enable console window for Windows