# Changelog

## v1.5.7 beta (2023.12.08)

### Feature

- Allow particle logger to record only absorbed hits

### Interface

- Support for progressbars with wide/multiline status
- Added few missing options to Global Settings

### Bugfix

- Fixed a few OpenGL crashes related to camera views
- Fixed convergence plotter interface elements becoming black rectangles
- Fixed crash at "auto-create difference of 2 facets" function
- Fixed crash at 0-depth geometries at certain camera angles
- MPI version compiles again (although Synrad not tested with MPI yet)
- Better flux/power estimation for particle logger export

## v1.5.6 beta (2023.02.17)
### Bugfix
- Fixed saving textures with wrong facet index
- Fixed CLI compression of syn7z files
- Fixed Undo command for Split, Build intersection and Align
- Fixed spectrum saving
### Interface
- Better log language
- Enable console window for Windows

## v1.5.5 beta (2023.01.17)
### Bugfix
- File->Insert menu now works.
- Fixed bug that saved convergence values once per each facet.

## v1.5.4 beta (2023.01.10)
### Feature
- Add S global coordinate to regions
- Allow to refer BXY file to global S beam coordinate
- Allow global and local magnetic field component definition (see docs)
### Bugfix
- Normalize starting direction vector to 1 if not done by user

## v1.5.3 beta (2022.12.16)
### Interface
- New Global Settings layout unified with Molflow
- Non-planar facet higlighting can be toggled
### Bugfix
- Not retexturing all facets for geometry operations (slow)
- Fix crash when creating difference of holed facets
- 

## v1.5.2 beta (2022.12.09)
### Bugfix
- Fixed crash when only regions are in the geometry
- Removed region view selector window as caused crashes
- Fixed OpenGL crash when geometry has 0-width in one dimension
- Fixed memory leak when collapsing facets
- Fixed 100% CPU usage when simulation paused
- Fixed app updater checking for debian versions on fedora

## v1.5.1 beta (2022.08.17)
### Bugfix
- Collinearity condition fixed (no more false NULL facets)
- Create facet with convex hull fixed, no more duplicate vertices
- Fix incorrect texture export

## v1.5.0 beta (2022.06.23)
### Feature
- Add Command Line Interface support (synradCLI)
- Add new Ray Tracing engine
- File-specific reflection and low-flux settings
- Updated SYN file version (v12)
- IMGui test windows integration
### Bugfix
- SR flux relative to local magnetic field, not global Y coordinate
- Correct export for part of the geometry (File->Save selected facets)
- Chose looser threshold for collienarity detection (null facets)

## v1.4.30 (2022.03.08)
### Feature
- Support for non-square textures
### Bugfix
- Load STL files works again

## v1.4.29 (2021.02.04)
### Feature
- Added Convergence plotter
- Support for multiple bodies in one STL file
### Interface
- Fix for mesh mixed state
- Allow dragging with D key and left mouse button hold
- Allow zooming with Z key and left mouse button hold
- "Select plotted" button in profile plotter
### Bugfix
- Closing collapse/smart select windows aborts the process
- More verbose error message in case of Subprocess error
- Crash when deleting a structure which is selected
- Restore shift+scroll for Mac
- Fixed timeout when loading large files