# Changelog 'v1.4.32'

### Bugfix
- Fixed spectrum saving
- Undo command works again (build intersection, split)
- STL insert fix, display collapse dialog