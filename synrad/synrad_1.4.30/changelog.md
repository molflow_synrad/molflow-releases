Synrad 1.4.30 (2021-02-17)

# Feature:

- Support for non-square textures

# Bugfix:

- Load STL files works again